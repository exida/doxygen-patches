Doxygen patched features:
=========================
  
- generating dot graph with additional vertex color support. Two types of nodes are supported: DotNode and DirDef. There have been applied additional naive 'absolute path' argument support in DotNode. It works fine with all known cases so far, but has not been statically verified so there might be some issues. There is no guarantee for this code.
- code compiles under cygwin environment (c++-4.9.3, cmake, python, flex, bison)
- currently using custom java wrapper to covert paths to windows format:
```
#!/bin/bash
CMD="%JAVA_HOME%/bin/java.exe "

for a in $*; do
  if [[ $a = /cygdrive* ]]; then
    CMD="$CMD `cygpath -w $a`"
  else
    CMD="$CMD $a"
  fi
done
echo $CMD > java.bat
chmod +x java.bat
./java.bat
```
- graph vertex are colored in following manner:
 -- if there is a exact rule the color is chosen without further checks,
 -- otherwise label/assumed_path is matched against regular expression
- TODO: BE CAREFUL: the color rules are applied weakly, and there is no strict rules in case of multiple matching regex, or using default color by exact rule.
- definition of rules should be provided in regular doxygen config as follows:
```
  DOT_COLOR_RULES_EXACT = label=color \
                          anotherlabel=anothercolor

  DOT_COLOR_RULES_REGEX = regex=color \
                          anotherregex=anothercolor
```
- Visual Studio compilation: needs investigation - same code compiled with msvc hit runtime errors (segfault). (the following software components are needed: python, cmake, win-flex, win-bison)
- Cygwin/g++ works out of the box
- using provided binary has been successfully tested in windows8 64b environment with preinstalled original doxygen v. 1.8.9.1, simply replaced the original binary
- in case any problems/feature request/commit please contact aleksandergajewski@gmail.com

