diff --git a/src/config.xml b/src/config.xml
index acbee8e..185480d 100644
--- a/src/config.xml
+++ b/src/config.xml
@@ -3153,6 +3153,37 @@ to be found in the default search path.
 ]]>
       </docs>
     </option>
+
+      <option type='list' id='DOT_COLOR_RULES_EXACT' format='string'>
+      <docs>
+          <![CDATA[
+ This tag can be used to specify a color rules for labels in dot graphs. A rule has the form:
+\verbatim
+ label=fillcolor
+\endverbatim
+ Example:
+\verbatim
+ "iostream=orange\n"
+\endverbatim
+]]>
+      </docs>
+    </option>
+
+      <option type='list' id='DOT_COLOR_RULES_REGEX' format='string'>
+      <docs>
+          <![CDATA[
+ This tag can be used to specify a color rules for labels in dot graphs. A rule has the form:
+\verbatim
+ labelregex=fillcolor
+\endverbatim
+ Example:
+\verbatim
+ "*cpp=orange\n"
+\endverbatim
+]]>
+      </docs>
+    </option>
+
     <option type='int' id='DOT_FONTSIZE' minval='4' maxval='24' defval='10' depends='HAVE_DOT'>
       <docs>
 <![CDATA[
diff --git a/src/dirdef.cpp b/src/dirdef.cpp
index 067daa0..9cd9684 100644
--- a/src/dirdef.cpp
+++ b/src/dirdef.cpp
@@ -40,7 +40,8 @@ DirDef::DirDef(const char *path) : Definition(path,1,1,path), visited(FALSE)
   { // strip trailing /
     m_dispName = m_dispName.left(m_dispName.length()-1);
   }
-  
+
+  m_path = path;
   m_fileList   = new FileList;
   m_usedDirs   = new QDict<UsedDir>(257);
   m_usedDirs->setAutoDelete(TRUE);
diff --git a/src/dirdef.h b/src/dirdef.h
index 1a87f5b..2de1135 100644
--- a/src/dirdef.h
+++ b/src/dirdef.h
@@ -48,6 +48,7 @@ class DirDef : public Definition
     virtual ~DirDef();
 
     // accessors
+    QCString getPath() const { return m_path; }
     DefType definitionType() const { return TypeDir; }
     QCString getOutputFileBase() const;
     QCString anchor() const { return QCString(); }
@@ -97,6 +98,7 @@ class DirDef : public Definition
     void computeDependencies();
 
     DirList m_subdirs;
+    QCString m_path;
     QCString m_dispName;
     QCString m_shortName;
     QCString m_diskName;
diff --git a/src/dot.cpp b/src/dot.cpp
index 705aa27..72e9144 100644
--- a/src/dot.cpp
+++ b/src/dot.cpp
@@ -24,6 +24,7 @@
 #include <qthread.h>
 #include <qmutex.h>
 #include <qwaitcondition.h>
+#include <iostream>
 
 #include "dot.h"
 #include "doxygen.h"
@@ -54,6 +55,106 @@
 #define FONTNAME getDotFontName()
 #define FONTSIZE getDotFontSize()
 
+
+
+
+namespace DotColorRules {
+
+    StringDict readDict(QCString configName) {
+      // add aliases to a dictionary
+      StringDict stringDict;
+      QStrList &stringList = Config_getList(configName);
+      const char *s = stringList.first();
+      stringDict.setAutoDelete(TRUE);
+      while (s) {
+        if (stringDict[s] == 0) {
+          QCString alias = s;
+          int i = alias.find('=');
+          if (i > 0) {
+            QCString name = alias.left(i).stripWhiteSpace();
+            QCString value = alias.right(alias.length() - i - 1);
+            //printf("Alias: found name=`%s' value=`%s'\n",name.data(),value.data());
+            if (!name.isEmpty()) {
+              QCString *dn = stringDict[name];
+              if (dn == 0) // insert new alias
+              {
+                stringDict.insert(name, new QCString(value));
+              }
+              else // overwrite previous alias
+              {
+                *dn = value;
+              }
+            }
+          }
+        }
+        s = stringList.next();
+      }
+
+      return stringDict;
+    }
+
+    QCString getColor(const QCString &label, const QCString &defaultColor) {
+
+      static StringDict exact = readDict("DOT_COLOR_RULES_EXACT");
+      static StringDict regex = readDict("DOT_COLOR_RULES_REGEX");
+
+      QCString ret = defaultColor;
+
+      //std::cout << "DOT_COLOR_RULES: getting color for label " << label << "." << std::endl;
+
+      if (exact[label] != 0) {
+        //std::cout << "DOT_COLOR_RULES_EXACT: using " << *exact[label] << " color for " << label << "." << std::endl;
+        ret = *exact[label];
+      } else {
+        QDictIterator<QCString> it(regex);
+
+        for (; it.current(); ++it) {
+          QRegExp qr(it.currentKey());
+          //std::cout << "DOT_COLOR_RULES_REGEX: matching label " << label << " with " << it.currentKey() << "." << std::endl;
+          if (qr.find(label, 0) >= 0) {
+            //std::cout << "DOT_COLOR_RULES_REGEX: MATCH - using " << *it.current() << " color for " << label << "." << std::endl;
+            ret = *it.current();
+          }
+        }
+      }
+
+      return ret;
+    }
+
+    QCString getColor(const DirDef *dirdef, const char * defaultColor) {
+
+        QCString ret(defaultColor);
+
+        //std::cout << "DOT_COLOR_RULES: getting color for " << dirdef->shortName() << " DirRef (assumed path: " << ((dirdef->getPath()) ? dirdef->getPath() : "MISSING") << ")." << std::endl;
+
+        ret = getColor(dirdef->shortName(), defaultColor);
+        if (ret == defaultColor) {
+          ret = getColor(dirdef->getPath(), defaultColor);
+        }
+
+        return ret;
+    }
+
+    QCString getColor(const DotNode * dn, const char * defaultColor) {
+
+      QCString ret(defaultColor);
+
+      //std::cout << "DOT_COLOR_RULES: getting color for " << dn->getLabel() << " DotNode (assumed path: " << ((dn->getPath()) ? dn->getPath() : "MISSING") << ")." << std::endl;
+
+      ret = getColor(dn->getLabel(), defaultColor);
+      if (ret == defaultColor) {
+          if (dn->getPath()) {
+            ret = getColor(dn->getPath(), defaultColor);
+          } else {
+              //std::cout << "DOT_COLOR_RULES: assuming " << dn->getLabel() << " is an external node with default color." << std::endl;
+          }
+      }
+
+      return ret;
+    }
+}
+
+
 //--------------------------------------------------------------------
 
 static const char svgZoomHeader[] =
@@ -219,7 +320,7 @@ struct EdgeProperties
   const char * const *edgeStyleMap;
 };
 
-static EdgeProperties normalEdgeProps = 
+static EdgeProperties normalEdgeProps =
 {
   normalEdgeColorMap, normalArrowStyleMap, normalEdgeStyleMap
 };
@@ -1474,6 +1575,13 @@ DotNode::DotNode(int n,const char *lab,const char *tip, const char *url,
   , m_truncated(Unknown)
   , m_distance(1000)
 {
+    if (cd) {
+        if (cd->getFileDef()) {
+            if (cd->getFileDef()->getPath()) {
+                m_path = cd->getFileDef()->getPath();
+            }
+        }
+    }
 }
 
 DotNode::~DotNode()
@@ -1821,7 +1929,9 @@ void DotNode::writeBox(FTextStream &t,
   t << "\",height=0.2,width=0.4";
   if (m_isRoot)
   {
-    t << ",color=\"black\", fillcolor=\"grey75\", style=\"filled\", fontcolor=\"black\"";
+    t << ",color=\"black\", fillcolor=\"";
+      t << DotColorRules::getColor(this, "white");
+      t << "\", style=\"filled,bold\", fontcolor=\"black\"";
   }
   else 
   {
@@ -1829,7 +1939,7 @@ void DotNode::writeBox(FTextStream &t,
     if (!dotTransparent)
     {
       t << ",color=\"" << labCol << "\", fillcolor=\"";
-      t << "white";
+      t << DotColorRules::getColor(this, "white");
       t << "\", style=\"filled\"";
     }
     else
@@ -2450,6 +2560,10 @@ void DotGfxHierarchyTable::addHierarchy(DotNode *n,ClassDef *cd,bool hideSuper)
               tooltip,
               tmp_url.data()
               );
+          if (bClass->getFileDef()) {
+              bn->setPath(bClass->getFileDef()->getPath());
+          }
+
           n->addChild(bn,bcd->prot);
           bn->addParent(n);
           //printf("  Adding node %s to new base node %s (c=%d,p=%d)\n",
@@ -2506,6 +2620,10 @@ void DotGfxHierarchyTable::addClassList(ClassSDict *cl)
           tooltip,
           tmp_url.data());
 
+      if(cd->getFileDef()) {
+          n->setPath(cd->getFileDef()->getPath());
+      }
+
       //m_usedNodes->clear();
       m_usedNodes->insert(cd->name(),n);
       m_rootNodes->insert(0,n);   
@@ -2643,6 +2761,7 @@ void DotClassGraph::addClass(ClassDef *cd,DotNode *n,int prot,
         tmp_url+="#"+cd->anchor();
       }
     }
+
     QCString tooltip = cd->briefDescriptionAsTooltip();
     bn = new DotNode(m_curNodeNumber++,
         displayName,
@@ -3394,6 +3513,11 @@ void DotInclDepGraph::buildGraph(DotNode *n,FileDef *fd,int distance)
               FALSE,             // rootNode
               0                  // cd
               );
+
+          if (bfd) {
+              bn->setPath(bfd->getPath());
+          }
+
           n->addChild(bn,0,0,0);
           bn->addParent(n);
           m_usedNodes->insert(in,bn);
@@ -3468,6 +3592,9 @@ DotInclDepGraph::DotInclDepGraph(FileDef *fd,bool inverse)
                             tmp_url.data(),
                             TRUE     // root node
                            );
+
+  m_startNode->setPath(fd->getPath());
+
   m_startNode->setDistance(0);
   m_usedNodes = new QDict<DotNode>(1009);
   m_usedNodes->insert(fd->absFilePath(),m_startNode);
@@ -3711,6 +3838,12 @@ void DotCallGraph::buildGraph(DotNode *n,MemberDef *md,int distance)
               uniqueId,
               0 //distance
               );
+
+          if (rmd->getFileDef()) {
+              bn->setPath(rmd->getFileDef()->getPath());
+          }
+          //std::cout << "<< build call <<" << linkToText(rmd->getLanguage(),name,FALSE) << " " << bn->getPath() << std::endl;
+
           n->addChild(bn,0,0,0);
           bn->addParent(n);
           bn->setDistance(distance);
@@ -3797,6 +3930,11 @@ DotCallGraph::DotCallGraph(MemberDef *md,bool inverse)
                             uniqueId.data(),
                             TRUE     // root node
                            );
+
+  if (md->getFileDef()) {
+      m_startNode->setPath(md->getFileDef()->getPath());
+  }
+
   m_startNode->setDistance(0);
   m_usedNodes = new QDict<DotNode>(1009);
   m_usedNodes->insert(uniqueId,m_startNode);
@@ -4307,6 +4445,7 @@ DotGroupCollaboration::DotGroupCollaboration(GroupDef* gd)
     QCString tmp_url = gd->getReference()+"$"+gd->getOutputFileBase();
     m_usedNodes = new QDict<DotNode>(1009);
     m_rootNode = new DotNode(m_curNodeId++, gd->groupTitle(), "", tmp_url, TRUE );
+
     m_rootNode->markAsVisible();
     m_usedNodes->insert(gd->name(), m_rootNode );
     m_edges.setAutoDelete(TRUE);
@@ -4769,107 +4908,117 @@ void writeDotDirDepGraph(FTextStream &t,DirDef *dd)
     t << "digraph \"" << dd->displayName() << "\" {\n";
     if (Config_getBool("DOT_TRANSPARENT"))
     {
-      t << "  bgcolor=transparent;\n";
+        t << "  bgcolor=transparent;\n";
     }
     t << "  compound=true\n";
     t << "  node [ fontsize=\"" << FONTSIZE << "\", fontname=\"" << FONTNAME << "\"];\n";
     t << "  edge [ labelfontsize=\"" << FONTSIZE << "\", labelfontname=\"" << FONTNAME << "\"];\n";
 
     QDict<DirDef> dirsInGraph(257);
-    
+
     dirsInGraph.insert(dd->getOutputFileBase(),dd);
     if (dd->parent())
     {
-      t << "  subgraph cluster" << dd->parent()->getOutputFileBase() << " {\n";
-      t << "    graph [ bgcolor=\"#ddddee\", pencolor=\"black\", label=\"" 
-        << dd->parent()->shortName() 
+        t << "  subgraph cluster" << dd->parent()->getOutputFileBase() << " {\n";
+        t << "    graph [ bgcolor=\"";
+        t << DotColorRules::getColor(dd->parent(), "#ddddee");
+        t << "\", pencolor=\"black\", label=\""
+        << dd->parent()->shortName()
         << "\" fontname=\"" << FONTNAME << "\", fontsize=\"" << FONTSIZE << "\", URL=\"";
-      t << dd->parent()->getOutputFileBase() << Doxygen::htmlFileExtension;
-      t << "\"]\n";
+        t << dd->parent()->getOutputFileBase() << Doxygen::htmlFileExtension;
+        t << "\"]\n";
     }
     if (dd->isCluster())
     {
-      t << "  subgraph cluster" << dd->getOutputFileBase() << " {\n";
-      t << "    graph [ bgcolor=\"#eeeeff\", pencolor=\"black\", label=\"\""
-        << " URL=\"" << dd->getOutputFileBase() << Doxygen::htmlFileExtension 
+        t << "  subgraph cluster" << dd->getOutputFileBase() << " {\n";
+        t << "    graph [ bgcolor=\"";
+        t << DotColorRules::getColor(dd, "#eeeeff");
+        t << "\", pencolor=\"black\", label=\"\""
+        << " URL=\"" << dd->getOutputFileBase() << Doxygen::htmlFileExtension
         << "\"];\n";
-      t << "    " << dd->getOutputFileBase() << " [shape=plaintext label=\"" 
+        t << "    " << dd->getOutputFileBase() << " [shape=plaintext label=\""
         << dd->shortName() << "\"];\n";
 
-      // add nodes for sub directories
-      QListIterator<DirDef> sdi(dd->subDirs());
-      DirDef *sdir;
-      for (sdi.toFirst();(sdir=sdi.current());++sdi)
-      {
-        t << "    " << sdir->getOutputFileBase() << " [shape=box label=\"" 
-          << sdir->shortName() << "\"";
-        if (sdir->isCluster())
+        // add nodes for sub directories
+        QListIterator<DirDef> sdi(dd->subDirs());
+        DirDef *sdir;
+        for (sdi.toFirst();(sdir=sdi.current());++sdi)
         {
-          t << " color=\"red\"";
-        }
-        else
-        {
-          t << " color=\"black\"";
+            t << "    " << sdir->getOutputFileBase() << " [shape=box label=\""
+            << sdir->shortName() << "\"";
+            if (sdir->isCluster())
+            {
+                t << " color=\"red\"";
+            }
+            else
+            {
+                t << " color=\"black\"";
+            }
+            t << " fillcolor=\"";
+            t << DotColorRules::getColor(sdir, "white");
+            t << "\" style=\"filled\"";
+            t << " URL=\"" << sdir->getOutputFileBase()
+            << Doxygen::htmlFileExtension << "\"";
+            t << "];\n";
+            dirsInGraph.insert(sdir->getOutputFileBase(),sdir);
         }
-        t << " fillcolor=\"white\" style=\"filled\"";
-        t << " URL=\"" << sdir->getOutputFileBase() 
-          << Doxygen::htmlFileExtension << "\"";
-        t << "];\n";
-        dirsInGraph.insert(sdir->getOutputFileBase(),sdir);
-      }
-      t << "  }\n";
+        t << "  }\n";
     }
     else
     {
-      t << "  " << dd->getOutputFileBase() << " [shape=box, label=\"" 
-        << dd->shortName() << "\", style=\"filled\", fillcolor=\"#eeeeff\","
-        << " pencolor=\"black\", URL=\"" << dd->getOutputFileBase() 
+        t << "  " << dd->getOutputFileBase() << " [shape=box, label=\""
+        << dd->shortName() << "\", style=\"filled\", fillcolor=\"";
+        t << DotColorRules::getColor(dd, "white");
+        t << "\","
+        << " pencolor=\"black\", URL=\"" << dd->getOutputFileBase()
         << Doxygen::htmlFileExtension << "\"];\n";
     }
     if (dd->parent())
     {
-      t << "  }\n";
+        t << "  }\n";
     }
 
     // add nodes for other used directories
     QDictIterator<UsedDir> udi(*dd->usedDirs());
     UsedDir *udir;
     //printf("*** For dir %s\n",shortName().data());
-    for (udi.toFirst();(udir=udi.current());++udi) 
-      // for each used dir (=directly used or a parent of a directly used dir)
+    for (udi.toFirst();(udir=udi.current());++udi)
+        // for each used dir (=directly used or a parent of a directly used dir)
     {
-      const DirDef *usedDir=udir->dir();
-      DirDef *dir=dd;
-      while (dir)
-      {
-        //printf("*** check relation %s->%s same_parent=%d !%s->isParentOf(%s)=%d\n",
-        //    dir->shortName().data(),usedDir->shortName().data(),
-        //    dir->parent()==usedDir->parent(),
-        //    usedDir->shortName().data(),
-        //    shortName().data(),
-        //    !usedDir->isParentOf(this)
-        //    );
-        if (dir!=usedDir && dir->parent()==usedDir->parent() && 
-            !usedDir->isParentOf(dd))
-          // include if both have the same parent (or no parent)
+        const DirDef *usedDir=udir->dir();
+        DirDef *dir=dd;
+        while (dir)
         {
-          t << "  " << usedDir->getOutputFileBase() << " [shape=box label=\"" 
-            << usedDir->shortName() << "\"";
-          if (usedDir->isCluster())
-          {
-            if (!Config_getBool("DOT_TRANSPARENT"))
+            //printf("*** check relation %s->%s same_parent=%d !%s->isParentOf(%s)=%d\n",
+            //    dir->shortName().data(),usedDir->shortName().data(),
+            //    dir->parent()==usedDir->parent(),
+            //    usedDir->shortName().data(),
+            //    shortName().data(),
+            //    !usedDir->isParentOf(this)
+            //    );
+            if (dir!=usedDir && dir->parent()==usedDir->parent() &&
+                !usedDir->isParentOf(dd))
+                // include if both have the same parent (or no parent)
             {
-              t << " fillcolor=\"white\" style=\"filled\"";
+                t << "  " << usedDir->getOutputFileBase() << " [shape=box label=\""
+                << usedDir->shortName() << "\"";
+                if (usedDir->isCluster())
+                {
+                    if (!Config_getBool("DOT_TRANSPARENT"))
+                    {
+                        t << " fillcolor=\"";
+                        t << DotColorRules::getColor(usedDir, "white");
+                        t << "\" style=\"filled\"";
+                    }
+                    t << " color=\"red\"";
+                }
+                t << " URL=\"" << usedDir->getOutputFileBase()
+                << Doxygen::htmlFileExtension << "\"];\n";
+                dirsInGraph.insert(usedDir->getOutputFileBase(),usedDir);
+                break;
             }
-            t << " color=\"red\"";
-          }
-          t << " URL=\"" << usedDir->getOutputFileBase() 
-            << Doxygen::htmlFileExtension << "\"];\n";
-          dirsInGraph.insert(usedDir->getOutputFileBase(),usedDir);
-          break;
+            dir=dir->parent();
         }
-        dir=dir->parent();
-      }
     }
 
     // add relations between all selected directories
@@ -4877,33 +5026,33 @@ void writeDotDirDepGraph(FTextStream &t,DirDef *dd)
     QDictIterator<DirDef> di(dirsInGraph);
     for (di.toFirst();(dir=di.current());++di) // foreach dir in the graph
     {
-      QDictIterator<UsedDir> udi(*dir->usedDirs());
-      UsedDir *udir;
-      for (udi.toFirst();(udir=udi.current());++udi) // foreach used dir
-      {
-        const DirDef *usedDir=udir->dir();
-        if ((dir!=dd || !udir->inherited()) &&     // only show direct dependendies for this dir
-            (usedDir!=dd || !udir->inherited()) && // only show direct dependendies for this dir
-            !usedDir->isParentOf(dir) &&             // don't point to own parent
-            dirsInGraph.find(usedDir->getOutputFileBase())) // only point to nodes that are in the graph
+        QDictIterator<UsedDir> udi(*dir->usedDirs());
+        UsedDir *udir;
+        for (udi.toFirst();(udir=udi.current());++udi) // foreach used dir
         {
-          QCString relationName;
-          relationName.sprintf("dir_%06d_%06d",dir->dirCount(),usedDir->dirCount());
-          if (Doxygen::dirRelations.find(relationName)==0)
-          {
-            // new relation
-            Doxygen::dirRelations.append(relationName,
-                new DirRelation(relationName,dir,udir));
-          }
-          int nrefs = udir->filePairs().count();
-          t << "  " << dir->getOutputFileBase() << "->" 
-                    << usedDir->getOutputFileBase();
-          t << " [headlabel=\"" << nrefs << "\", labeldistance=1.5";
-          t << " headhref=\"" << relationName << Doxygen::htmlFileExtension 
-            << "\"];\n";
+            const DirDef *usedDir=udir->dir();
+            if ((dir!=dd || !udir->inherited()) &&     // only show direct dependendies for this dir
+                (usedDir!=dd || !udir->inherited()) && // only show direct dependendies for this dir
+                !usedDir->isParentOf(dir) &&             // don't point to own parent
+                dirsInGraph.find(usedDir->getOutputFileBase())) // only point to nodes that are in the graph
+            {
+                QCString relationName;
+                relationName.sprintf("dir_%06d_%06d",dir->dirCount(),usedDir->dirCount());
+                if (Doxygen::dirRelations.find(relationName)==0)
+                {
+                    // new relation
+                    Doxygen::dirRelations.append(relationName,
+                                                 new DirRelation(relationName,dir,udir));
+                }
+                int nrefs = udir->filePairs().count();
+                t << "  " << dir->getOutputFileBase() << "->"
+                << usedDir->getOutputFileBase();
+                t << " [headlabel=\"" << nrefs << "\", labeldistance=1.5";
+                t << " headhref=\"" << relationName << Doxygen::htmlFileExtension
+                << "\"];\n";
+            }
         }
-      }
     }
 
     t << "}\n";
-}
+}
\ No newline at end of file
diff --git a/src/dot.h b/src/dot.h
index 3de7d79..4886052 100644
--- a/src/dot.h
+++ b/src/dot.h
@@ -89,6 +89,9 @@ class DotNode
     bool isVisible() const { return m_visible; }
     TruncState isTruncated() const { return m_truncated; }
     int distance() const { return m_distance; }
+    void setPath(const QCString &path) { m_path = path; }
+    QCString getPath() const { return m_path; }
+    QCString getLabel() const { return m_label; }
 
   private:
     void colorConnectedNodes(int curColor);
@@ -115,6 +118,7 @@ class DotNode
     bool             m_visible;   //!< is the node visible in the output
     TruncState       m_truncated; //!< does the node have non-visible children/parents
     int              m_distance;  //!< shortest path to the root node
+    QCString         m_path;
 
     friend class DotGfxHierarchyTable;
     friend class DotClassGraph;
diff --git a/src/doxygen.cpp b/src/doxygen.cpp
index 4ff533d..979dc99 100644
--- a/src/doxygen.cpp
+++ b/src/doxygen.cpp
@@ -9959,6 +9959,7 @@ static void devUsage()
 static void usage(const char *name)
 {
   msg("Doxygen version %s\nCopyright Dimitri van Heesch 1997-2015\n\n",versionString);
+  msg("Patched with support for: \n\tDOT_COLOR_RULES_EXACT\n\tand\n\tDOT_COLOR_RULES_REGEX\n\n");
   msg("You can use doxygen in a number of ways:\n\n");
   msg("1) Use doxygen to generate a template configuration file:\n");
   msg("    %s [-s] -g [configName]\n\n",name);
diff --git a/src/to_c_cmd.py b/src/to_c_cmd.py
index 52785f1..ad2a1b3 100755
--- a/src/to_c_cmd.py
+++ b/src/to_c_cmd.py
@@ -5,4 +5,5 @@
 # place an escaped \n and " at the end of each line
 import sys
 for line in sys.stdin:
-    sys.stdout.write('"' + line.replace('\\','\\\\').replace('"','\\"').replace('\n','') + '\\n"\n')
+    sys.stdout.write('"' + line.replace('\\','\\\\').replace('"','\\"').rstrip() + '"\n')
+#    sys.stdout.write('"' + line + '"')
